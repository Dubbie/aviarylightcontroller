#include <RGBLed.h>
#include <EasyButton.h>
#include <StateMachine.h>

////////////////////////////
// Adjust parameters here:
////////////////////////////

#define startMins             30   // The lower value of the time range pot in minutes
#define endMins               180  // The upper value of the time range pot in minutes
#define fadeupTimeSecs        120  // The time that the light will spend fading up in seconds
#define fadedownTimeMins      30   // The time that the light will spend fading down in minutes
#define blinkTimeMs           200  // The time interval of the LED blink for test mode.
#define nightfalltHoldoffMins 1   // The time elapsed before the nightfall signal is accepted as valid 
#define daybreakHoldoffMins   30   // The time elapsed before night/day transition is accepted as valid
#define gamma                 2.4  // Applies a perceptual remapping to the light fades - they start slow and speed up. For pure linear mapping set to 1.0

////////////////////////////

#define RED_PIN     16
#define GREEN_PIN   17
#define BLUE_PIN    20
#define LED_PIN     13
#define SSR_PIN     23
#define SWTest_PIN  14
#define SWNext_PIN  15
#define Day_PIN     21

const unsigned long rangeMins = endMins - startMins;
const unsigned long fadeupTimeMillis = fadeupTimeSecs * 1000;
const unsigned long fadedownTimeMillis = fadedownTimeMins * 60 * 1000;
const unsigned long nightfalltHoldoffMillis = nightfalltHoldoffMins * 60 * 1000;
const unsigned long daybreakHoldoffMillis = daybreakHoldoffMins * 60 * 1000;

RGBLed LED(RED_PIN, GREEN_PIN, BLUE_PIN, RGBLed::COMMON_CATHODE);
EasyButton nextButton(SWNext_PIN, 40, false, true);
EasyButton testButton(SWTest_PIN, 40, false, true);

StateMachine machine = StateMachine();

State* S0_daytime   = machine.addState(&daytime_0);
State* S1_fadeUp    = machine.addState(&fadeUp_1);
State* S2_lightsOn  = machine.addState(&lightsOn_2);
State* S3_fadeDown  = machine.addState(&fadeDown_3);
State* S4_nighttime = machine.addState(&nighttime_4);
State* S5_Test      = machine.addState(&test_5);

int savedState = 0;  // save the current state for when we go into test mode, so that we can return to the same state.

unsigned long fadeupTimeStamp = 0;
unsigned long fadedownTimeStamp = 0;
unsigned long blinkTimeStamp = 0;
unsigned long lightsOnTimeStamp = 0;
unsigned long nightfallTimeStamp = 0;
unsigned long daybreakTimeStamp = 0;
bool LEDState = LOW;
bool DaylightState = LOW;

void setSSR(bool state);
bool readDaylight(void);
unsigned long readPot(void);
void nextState(void);


////// Functions to process each of the states //////

void daytime_0(){                                                             
  
  if(machine.executeOnce){
    LED.brightness(RGBLed::RED, 40);
    setSSR(LOW);
    nightfallTimeStamp = millis();
    analogWrite(A12, 409); // Set output to 1V
  }

  if (readDaylight() == LOW) {
    if ((millis() - nightfallTimeStamp) > nightfalltHoldoffMillis) {
      machine.transitionTo(S1_fadeUp);                                 
    }
  } else {
    nightfallTimeStamp = millis();
  }
}

void fadeUp_1(){                                                              
  if(machine.executeOnce){
    fadeupTimeStamp = millis();
    setSSR(HIGH);
    LED.brightness(RGBLed::YELLOW, 30);
  }
  unsigned long elapsed = millis() - fadeupTimeStamp;
  float fadeVal = float(elapsed)/float(fadeupTimeMillis);
  if (fadeVal >= 1.0) {
    machine.transitionTo(S2_lightsOn);
  } else {
    int lampVal =  int(pow(fadeVal, gamma) * 3687 + 409);
    analogWrite(A12, lampVal);
  }
}

void lightsOn_2(){                                                             // Complete
  if(machine.executeOnce){
    lightsOnTimeStamp = millis();
    setSSR(HIGH);
    analogWrite(A12, 4096); // Set output to 10V
    LED.brightness(RGBLed::BLUE, 40);
  }
  if ((millis() - lightsOnTimeStamp) > readPot()) {
    machine.transitionTo(S3_fadeDown);
  }
}

void fadeDown_3(){
  if(machine.executeOnce){
    fadedownTimeStamp = millis();
    setSSR(HIGH);
    LED.brightness(RGBLed::MAGENTA, 20);
  }
  unsigned long elapsed = millis() - fadedownTimeStamp;
  float fadeVal = float(elapsed)/float(fadedownTimeMillis);
  fadeVal = 1.0 - fadeVal;
  if (fadeVal <= 0.0) {
    machine.transitionTo(S4_nighttime);
  } else {
    int lampVal = int(pow(fadeVal, gamma) * 3687 + 409);
    analogWrite(A12, lampVal);  
  }
}

void nighttime_4(){
  if(machine.executeOnce){
    setSSR(LOW);
    LED.brightness(RGBLed::GREEN, 20);
    daybreakTimeStamp = millis();
  }
  
  if (readDaylight() == HIGH) {
    if ((millis() - daybreakTimeStamp) > daybreakHoldoffMillis) {
      machine.transitionTo(S0_daytime);                                 
    }
  } else {
    daybreakTimeStamp = millis();
  }
}

void test_5(){
  
    if(machine.executeOnce){
    blinkTimeStamp = millis();
    LED.brightness(0, 255, 200, 20);
    LEDState = HIGH;

    setSSR(HIGH); // light power on
    analogWrite(A12, 4096); // Set output to 10V
  
    if ((millis() - blinkTimeStamp) > blinkTimeMs) {
      if (LEDState) {
        LED.off();
        LEDState = LOW;
        blinkTimeStamp = millis();
      } else {
        LED.brightness(0, 255, 200, 20);
        LEDState = HIGH;
        blinkTimeStamp = millis();
      }
    }
    if (!(testButton.isPressed())) {
      machine.transitionTo(savedState);
    }
  }
}

/////// End of state functions  /////////

void nextState(void) {
  machine.transitionTo((machine.currentState + 1) % 5);
}

unsigned long readPot(void) {
  int potVal = analogRead(A8);
  float amt = 1.0 - (float(potVal) / 1024.0);
  unsigned long mins = int((rangeMins * amt)) + startMins;
  return (mins * 60 * 1000);
}

bool readDaylight(void){
  return (digitalRead(Day_PIN) != LOW);
}

void setSSR(bool state) {
  digitalWrite(SSR_PIN, !state);
}
 
void setup() {
  
  pinMode(SSR_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(Day_PIN, INPUT);
    
  digitalWrite(LED_PIN, HIGH);
  setSSR(LOW);

  LED.brightness(RGBLed::RED, 40);
  nextButton.begin();
  testButton.begin();

  nextButton.onPressed(nextState);
  analogWriteResolution(12);
  Serial.begin(9600);
}

void loop() {
  
  machine.run();
  nextButton.read();
  testButton.read();
  
  if (testButton.isPressed()) {
    if (machine.currentState != 5) {
      savedState = machine.currentState;
      machine.transitionTo(S5_Test);
    }
  }

  //digitalWrite(LED_PIN, readDaylight());
}
