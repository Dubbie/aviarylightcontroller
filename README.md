# AviaryLightController

Light Controller for Cape Aviary.

![Board Image](AviaryLightController.jpg)

## Wiring:

- +12V  - Connect 12V DC power supply here. Max allowed voltage range 11.0V - 15.0V. **_THERE IS NO REVERSE POLARITY PROTECTION SO DON'T MIX THESE UP!_**
- GND   - Ground for system - connect power supply ground, daylight sensor ground and SSR ground here.
- SSR   - 12V output signal to SSR. High signal should switch SSR on.
- 1-10V - 1-10V brightness signal for LED lights. Should be able to chain multiple lights in PARALLEL
- Day   - Connect to a switch. Open means no daylight, closed means daylight. 12V signal.


## Controls:


1. Time adjustment pot
This sets the time delay for State 3 (lights on). The range is from 30 mins to 180 mins
This pot can be replaced with a remotely wired pot if desired. Use any value linear pot between 1k and 10K


2. Force next state button
Pressing this button manually advances the state to the next. After state 5, it wraps around to state 1 again.

3. Test button
Holding this button turns the SSR on and sets the brightness to 10V. Releasing the button returns you to the state from where you came. Timers for the state are reset.
While the button is held, the LED will flash CYAN on and off.

4. Reset button
Pressing this button resets the microcontroller.

There are 2 LEDs

5. The red LED on the microcontroller shows that it has power and is functioning.

6. The RGB LED shows what state the system is in, by colors listed below.

7. Calibration Trimpot. Trim this pot to calibrate the 1-10V signal to exactly 10V when the test button is held down. Should only need to be done once.

8. USB connector. plug in USB cable here to upload new firmware if necessary.

## Operation:

There are 5 main states of operation

**State 1: Daytime - led: RED**

When the daylight signal has been HIGH for at least two hours (to guard against a torchlight or something making the system think day has come.) we transition to the daytime state.
In this state, The SSR is off and the light fade set to zero.

**State 2: Fade Up Lights - led: YELLOW**

When the Daylight signal has been LOW for 10 minutes(?), we transition to the fade up state. The SSR is set HIGH and for 120 seconds, the lamp increases the fade value from 1.0 to 10.0V.
The Daylight signal is ignored in this state.

**State 3: Lights on - led: BLUE**

When the fade up from state 2 is complete, we enter state 3. SSR is HIGH and the fade value is 10V. A timer runs, counting up to the value set on the Time adjustment pot.
The Daylight signal is ignored in this state.

**State 4: Fade Down Lights - led: MAGENTA**

When the timer value in state 3 is reached, we enter state 4. SSR is HIGH. The lights fade down over 30 mins from 10.0V to 1.0V.
The Daylight signal is ignored in this state.

**State 5: Nighttime - led: GREEN**

When the fade down is complete, we transition to this state. SSR is LOW and fade value is 1.0V
After 2 hours in this state, the Daylight signal is activated again.


## Build instructions:

1. Install Arduino from here: https://www.arduino.cc/en/software

2. Install Teensyduino from here: https://www.pjrc.com/teensy/td_download.html

3. Launch Arduino IDE and select Tools > Manage Libraries. Search for "RGB" and scroll down untill you find a library called "RGB" by Steven Wilmouth. Click install.

4. in the same Library Manager, search for "StateMachine" and find the library by Jose Rullan. Install it.

5. Close the library manager.

6. Open the AviaryLightControl.ino file downloaded from this Git repository.

7. Under Tools > Board, check that "Teensy LC" is selected.

8. Plug in the board with a USB cable and press the upload button. All going well, the lights will blink on the board then turn solid red.

9. Unplug and you now have the latest firmware.

